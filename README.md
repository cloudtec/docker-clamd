# README #

### Output ###
Mime Type: text/plain

* OK -> negative result
* NOK -> positive result
* ERROR -> file not found

### Errors ###
Shows `413 Request Entity Too Large` error when the file is too large.
Check `client_max_body_size` and other related configuration options.

### Test form ###
```html
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Test</title>
</head>
<body>
<form method="POST" action="http://something" enctype="multipart/form-data">
    <input type="file" name="scan">
    <input type="submit">
</form>
</body>
</html>
```