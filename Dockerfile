FROM cloudtecbern/ep:latest

### FOLDERS + FILES ####################################################################################################

COPY ./scripts/scan.sh /scripts/
COPY ./scripts/index.php /usr/share/nginx/html/public/
COPY ./scripts/startup/99-clamd.sh /scripts/startup/
COPY ./conf/clamd.conf /etc/clamav/clamd.conf
COPY ./conf/freshclam.conf /etc/clamav/freshclam.conf

### INSTALL ############################################################################################################

RUN apk update && \
    apk add --no-cache clamav && \
    mkdir -p /run/clamav && \
    chmod 0777 /run/clamav && \
    chown default:default /run/clamav && \
    chown -R default:default /var/lib/clamav && \
    mkdir -p /var/log/clamav && \
    touch /var/log/clamav/clamav.log && \
    chown default:default /var/log/clamav/clamav.log