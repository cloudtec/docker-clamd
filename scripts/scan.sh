#!/bin/bash

#
# scan file ($1), echo ERROR (file not found), OK (negative result) or NOK (positive result)
#

# required parameter
if [[ "$1" == "" ]]; then
    echo "ERROR"
    exit
fi

# check if its a file
if [[ ! -f "$1" ]]; then
    echo "ERROR"
    exit
fi

# scan file
chmod 0777 $1
if ! clamdscan --quiet --fdpass --no-summary $1; then
    echo "NOK"
else
    echo "OK"
fi