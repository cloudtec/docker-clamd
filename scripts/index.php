<?php
/*
 * - get a file (path) from form post
 * - pass it to scan.sh
 * - output "OK" (negative),  "NOK" (positive) or "ERROR" (file not found)
 */

header('Content-Type: text/plain');

// check input

if (! is_array($_FILES)
    || ! count($_FILES)
    || ! array_key_exists('scan', $_FILES)
    || ! array_key_exists('tmp_name', $_FILES['scan'])
    || ! is_file($_FILES['scan']['tmp_name'])
) {
    echo "ERROR";
    exit;
}

if ($_FILES['scan']['error'] !== UPLOAD_ERR_OK) {
    echo "ERROR";
    exit;
}

// scan file

$output = [];
exec("/scripts/scan.sh {$_FILES['scan']['tmp_name']}", $output);

// write response

echo (count($output) && in_array($output[0], ['OK', 'NOK', 'ERROR']))
    ? $output[0]
    : 'ERROR';